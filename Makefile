CC=gcc
CFLAGS=-std=c99 -O2 -Wall -Werror -Wextra -pedantic -Iinclude
BIN=bin
INC=include
SRC=src
OUT=bash++

all: io main parser
	$(CC) $(CLFAGS) $(BIN)/io.o $(BIN)/main.o $(BIN)/parser.o -o $(OUT)

clean:
	@rm -rf $(BIN)
	@rm -rf $(OUT)
	@rm -rf html
	@rm -rf latex
	@rm -rf man

doc:
	doxygen Doxyfile

init:
	@mkdir -p $(BIN)

io: init
	$(CC) -c $(CFLAGS) $(SRC)/io.c -o $(BIN)/io.o

main: init
	$(CC) -c $(CFLAGS) $(SRC)/main.c -o $(BIN)/main.o

parser: init
	$(CC) -c $(CFLAGS) $(SRC)/parser.c -o $(BIN)/parser.o
