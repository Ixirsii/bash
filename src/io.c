/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           io.c                                                    **
 ** @brief          File input/output functions for bash++ major assignment **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <io.h>

#include <iso646.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <parser.h>


#define FTYPE ".bash"


int get_fname_from_bpp (ProgramData data, const char *suffix);
int get_fname_no_suff (ProgramData data);


/** @brief          Get the name of the bash output file.
 *
 *  @details        Parses the input file name provided it has a .bash++ suffix
 *                  and adds the output filename to data.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         0 if the name was successfully parsed.
 */
int
get_fname_from_bpp (ProgramData data, const char *suffix)
{
    size_t len = (suffix - data->i_name) + strlen (FTYPE);
    data->o_name = malloc (len);
    memcpy (data->o_name, data->i_name, (suffix - data->i_name));
    strcat (data->o_name, FTYPE);
    return 0;
}


/** @brief          Get the name of the bash output file.
 *
 *  @details        Parses the input file name provided it has no suffix and
 *                  adds the output filename to data.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         0 if the name was successfully parsed.
 */
int
get_fname_no_suff (ProgramData data)
{
    data->o_name = malloc (strlen (data->i_name) + strlen (FTYPE));
    strcpy (data->o_name, data->i_name);
    strcat (data->o_name, FTYPE);
    return 0;
}


/** @brief          Get the name of the bash output file.
 *
 *  @details        Parses the input file name and adds the output file name
 *                  to data.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         0 if the name was successfully parsed, or 1 if it wasn't.
 */
int
get_o_filename (ProgramData data)
{
    if (NULL == data) {
        printf ("NULL == data\n");
        return 1;
    }
    char *end = strstr (data->i_name, ".bash++");
    if (NULL == end)
        return get_fname_no_suff (data);
    else
        get_fname_from_bpp (data, end);
    return 0;
}

/** @brief          Parses the input file line by line and writes the parsed
 *                  line to the output file.
 *
 *  @details        Reads the input file a line at a time, parses the line, and
 *                  writes the resulting line to the output file.  This
 *                  function handles opening and closing the input and output
 *                  files and passes the FILE pointers to other functions.
 *                  By openening and closing the files in the same function we
 *                  make sure to not leak resources.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         0 if the input file was successfully parsed, and the
 *                  results written to the output file, or 1 if there was an
 *                  error.
 */
int
run (ProgramData data)
{
    char in_line[128];
    FILE *in, *out;
    in = fopen (data->i_name, "r");
    out = fopen (data->o_name, "w");
    while (NULL not_eq fgets (in_line, 128, in)){
        char o_line[128] = {0};
        parse_line (data, in_line, o_line);
        fprintf (out, "%s", o_line);
    }
    fclose (in);
    fclose (out);
    return 0;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
